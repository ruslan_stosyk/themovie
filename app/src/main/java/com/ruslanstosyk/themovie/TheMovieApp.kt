package com.ruslanstosyk.themovie

import android.app.Application
import com.ruslanstosyk.themovie.di.daoModule
import com.ruslanstosyk.themovie.di.loginModule
import com.ruslanstosyk.themovie.di.movieDetailsModule
import com.ruslanstosyk.themovie.di.networkModule
import com.ruslanstosyk.themovie.di.repositoryModule
import com.ruslanstosyk.themovie.di.searchMovieModule
import com.ruslanstosyk.themovie.di.splashModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class TheMovieApp : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@TheMovieApp)
            modules(
                listOf(
                    networkModule,
                    daoModule,
                    repositoryModule,
                    splashModule,
                    loginModule,
                    searchMovieModule,
                    movieDetailsModule
                )
            )
        }
    }
}