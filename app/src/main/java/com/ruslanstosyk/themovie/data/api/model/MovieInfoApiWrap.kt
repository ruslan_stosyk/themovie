package com.ruslanstosyk.themovie.data.api.model

import com.google.gson.annotations.SerializedName

data class MovieInfoApiWrap(
    @SerializedName("id") val id: Int,
    @SerializedName("poster_path") val posterPath: String?,
    @SerializedName("original_title") val originalTitle: String?
)