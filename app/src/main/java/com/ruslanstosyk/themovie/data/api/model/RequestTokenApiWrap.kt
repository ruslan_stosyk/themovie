package com.ruslanstosyk.themovie.data.api.model

import com.google.gson.annotations.SerializedName

data class RequestTokenApiWrap(
    @SerializedName("success") val success: Boolean,
    @SerializedName("expires_at") val expirationDate: String,
    @SerializedName("request_token") val requestToken: String
)