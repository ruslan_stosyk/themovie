package com.ruslanstosyk.themovie.data.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.ruslanstosyk.themovie.data.dao.model.MovieDaoWrap

@Dao
interface MovieDao {
    @Query("SELECT * from favorite_movies_table where id = :id LIMIT 1")
    suspend fun loadMovieById(id: Int): MovieDaoWrap?

    @Insert
    suspend fun insertMovie(movieDaoWrap: MovieDaoWrap)

    @Update
    suspend fun updateMovie(movieDaoWrap: MovieDaoWrap)

    @Delete
    suspend fun deleteMovie(movieDaoWrap: MovieDaoWrap)
}