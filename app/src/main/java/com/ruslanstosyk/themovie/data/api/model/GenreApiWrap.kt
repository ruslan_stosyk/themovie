package com.ruslanstosyk.themovie.data.api.model

import com.google.gson.annotations.SerializedName

data class GenreApiWrap(
    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String
)