package com.ruslanstosyk.themovie.data.dao.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "request_token_table")
class RequestTokenDaoWrap(@PrimaryKey var expirationTimestamp: Long, var requestToken: String)