package com.ruslanstosyk.themovie.data.dao.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "favorite_movies_table")
class MovieDaoWrap(@PrimaryKey val id: Int, val isFavorite: Boolean)