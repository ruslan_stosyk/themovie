package com.ruslanstosyk.themovie.data.api.model

import com.google.gson.annotations.SerializedName

data class ErrorRequestApiWrap(
    @SerializedName("status_message") val statusMessage: String,
    @SerializedName("status_code") val statusCode: Int
)