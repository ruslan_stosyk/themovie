package com.ruslanstosyk.themovie.data.api.model

import com.google.gson.annotations.SerializedName

data class MovieResponseApiWrap(
    @SerializedName("page") val page: Int,
    @SerializedName("total_results") val totalResults: Int,
    @SerializedName("total_pages") val totalPages: Int,
    @SerializedName("results") val movieInfoApiWrapList: List<MovieInfoApiWrap>
)