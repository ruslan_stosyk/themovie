package com.ruslanstosyk.themovie.data.api

import com.ruslanstosyk.themovie.data.api.model.MovieApiWrap
import com.ruslanstosyk.themovie.data.api.model.MovieResponseApiWrap
import com.ruslanstosyk.themovie.data.api.model.RequestTokenApiWrap
import com.ruslanstosyk.themovie.utils.RestApiUtils
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

interface RestApiService {
    companion object {
        private const val VERSION = RestApiUtils.VERSION
        const val AUTHENTICATION = "/authentication"
        const val TOKEN = "/token"
        const val NEW = "/new"
        const val LOGIN = "/validate_with_login"
        const val SEARCH = "/search"
        const val MOVIE = "/movie"
    }

    @GET("$VERSION$AUTHENTICATION$TOKEN$NEW")
    suspend fun getRequestTokenAsync(): Response<RequestTokenApiWrap>

    @POST("$VERSION$AUTHENTICATION$TOKEN$LOGIN")
    suspend fun loginAsync(
        @Query("username") userName: String,
        @Query("password") password: String,
        @Query("request_token") requestToken: String
    ): Response<RequestTokenApiWrap>

    @GET("$VERSION$SEARCH$MOVIE")
    suspend fun makeSearch(@Query("query") query: String): Response<MovieResponseApiWrap>

    @GET("$VERSION$MOVIE/{movie_id}")
    suspend fun getMovieById(@Path("movie_id") id: Int): Response<MovieApiWrap>
}