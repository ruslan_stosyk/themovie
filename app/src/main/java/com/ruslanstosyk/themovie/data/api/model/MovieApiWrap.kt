package com.ruslanstosyk.themovie.data.api.model

import com.google.gson.annotations.SerializedName

data class MovieApiWrap(
    @SerializedName("id") val id: Int,
    @SerializedName("poster_path") val posterPath: String?,
    @SerializedName("original_title") val originalTitle: String?,
    @SerializedName("overview") val overview: String?,
    @SerializedName("genres") val genres: List<GenreApiWrap>?,
    @SerializedName("release_date") val releaseDate: String?
)