package com.ruslanstosyk.themovie.data.model

data class Movie(
    val id: Int,
    val posterPath: String?,
    val originalTitle: String?,
    val overview: String?,
    val genres: List<String>?,
    val releaseDate: String?,
    val isFavorite: Boolean = false
)