package com.ruslanstosyk.themovie.data.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.ruslanstosyk.themovie.data.dao.model.RequestTokenDaoWrap

@Dao
interface RequestTokenDao {

    @Query("SELECT * FROM request_token_table")
    suspend fun getRequestToken(): List<RequestTokenDaoWrap>

    @Insert
    suspend fun insertRequestToken(requestTokenDaoWrap: RequestTokenDaoWrap)

    @Update
    suspend fun updateRequestToken(requestTokenDaoWrap: RequestTokenDaoWrap)

    @Delete
    suspend fun deleteRequestToken(requestTokenDaoWrap: RequestTokenDaoWrap)
}