package com.ruslanstosyk.themovie.data.model

data class MovieInfo(
    val id: Int,
    val posterPath: String,
    val originalTitle: String
)