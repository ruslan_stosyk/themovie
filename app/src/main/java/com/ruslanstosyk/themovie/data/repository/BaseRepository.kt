package com.ruslanstosyk.themovie.data.repository

import com.google.gson.Gson
import com.ruslanstosyk.themovie.data.api.model.ErrorRequestApiWrap
import com.ruslanstosyk.themovie.data.api.model.Result
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

abstract class BaseRepository {

    protected suspend fun <T : Any> getResult(call: suspend () -> Response<T>): Result<T> {
        try {
            val response = call()
            if (response.isSuccessful) {
                val body = response.body()
                if (body != null) return Result.Success(body)
            }
            val gson = Gson()
            val errorRequest: ErrorRequestApiWrap? = gson.fromJson(
                response.errorBody()!!.charStream(),
                ErrorRequestApiWrap::class.java
            )
            if (errorRequest != null) {
                return error(errorRequest.statusMessage)
            }
            return error(" ${response.code()} ${response.message()}")
        } catch (e: Exception) {
            return error(e.message ?: e.toString())
        }
    }

    private fun <T : Any> error(message: String): Result<T> {
        return Result.Error(message)
    }

    protected fun isTokenValid(timestamp: Long): Boolean {
        val expirationDate = Date(timestamp)
        val currentDate = Date(System.currentTimeMillis())
        return expirationDate.after(currentDate)
    }

    protected fun convertExpirationDateToTimestamp(expirationDate: String): Long {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss Z", Locale.ENGLISH)
        val parsedDate: Date = dateFormat.parse(expirationDate) ?: Date(0)
        return parsedDate.time
    }
}