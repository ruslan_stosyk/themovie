package com.ruslanstosyk.themovie.data.repository

import com.ruslanstosyk.themovie.data.model.AppResult
import kotlinx.coroutines.Deferred

interface AuthRepository {

    suspend fun loginAsync(username: String, password: String): Deferred<AppResult<Boolean>>

    suspend fun isLoggedInAsync(): Deferred<Boolean>
}