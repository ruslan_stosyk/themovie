package com.ruslanstosyk.themovie.data.repository

import com.ruslanstosyk.themovie.data.model.AppResult
import com.ruslanstosyk.themovie.data.model.Movie
import com.ruslanstosyk.themovie.data.model.MovieInfo
import kotlinx.coroutines.Deferred

interface MovieRepository {
    suspend fun makeSearchAsync(query: String): Deferred<AppResult<List<MovieInfo>>>

    suspend fun getMovieByIdAsync(movieId: Int): Deferred<AppResult<Movie>>
    suspend fun deleteFromFavorite(movieId: Int)
    suspend fun addToFavorite(movieId: Int)
}