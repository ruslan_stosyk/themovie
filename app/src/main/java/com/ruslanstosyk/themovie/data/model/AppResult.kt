package com.ruslanstosyk.themovie.data.model

data class AppResult<T>(
    val isSuccess: Boolean = true,
    val errorText: String? = null,
    val errorId: Int? = null,
    val subError: String? = null,
    val data: T?
)