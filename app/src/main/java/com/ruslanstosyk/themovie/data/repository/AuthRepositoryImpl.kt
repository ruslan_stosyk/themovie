package com.ruslanstosyk.themovie.data.repository

import com.ruslanstosyk.themovie.data.api.RestApiService
import com.ruslanstosyk.themovie.data.api.model.RequestTokenApiWrap
import com.ruslanstosyk.themovie.data.api.model.Result
import com.ruslanstosyk.themovie.data.dao.RequestTokenDao
import com.ruslanstosyk.themovie.data.dao.model.RequestTokenDaoWrap
import com.ruslanstosyk.themovie.data.model.AppResult
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.withContext

class AuthRepositoryImpl(
    private val restApiService: RestApiService,
    private val requestTokenDao: RequestTokenDao
) : BaseRepository(), AuthRepository {

    override suspend fun isLoggedInAsync() = withContext(Dispatchers.IO) {
        val requestTokenDaoWrapList = requestTokenDao.getRequestToken()
        val expirationTimestamp = requestTokenDaoWrapList
            .maxBy { it.expirationTimestamp }
            ?.expirationTimestamp ?: 0L
        return@withContext async { isTokenValid(expirationTimestamp) }
    }

    override suspend fun loginAsync(
        username: String,
        password: String
    ): Deferred<AppResult<Boolean>> = withContext(Dispatchers.IO) {
        return@withContext getInitialRequestTokenAsync(username, password)
    }

    private suspend fun getInitialRequestTokenAsync(
        username: String,
        password: String
    ): Deferred<AppResult<Boolean>> = withContext(Dispatchers.IO) {
        val response: Result<RequestTokenApiWrap> =
            getResult { restApiService.getRequestTokenAsync() }
        if (response is Result.Success) {
            return@withContext doLoginAsync(username, password, response.data.requestToken)
        } else {
            return@withContext async {
                AppResult(
                    isSuccess = false,
                    errorText = response.toString(),
                    data = false
                )
            }
        }
    }

    private suspend fun doLoginAsync(
        username: String,
        password: String,
        requestToken: String
    ): Deferred<AppResult<Boolean>> = withContext(Dispatchers.IO) {
        val response = getResult {
            restApiService.loginAsync(username, password, requestToken)
        }
        if (response is Result.Success) {
            return@withContext fetchRequestTokenAsync(response.data)
        } else {
            return@withContext async {
                AppResult(
                    isSuccess = false,
                    errorText = response.toString(),
                    data = false
                )
            }
        }
    }

    private suspend fun fetchRequestTokenAsync(
        requestTokenApiWrap: RequestTokenApiWrap
    ): Deferred<AppResult<Boolean>> = withContext(Dispatchers.IO) {

        val expirationTimeStamp =
            convertExpirationDateToTimestamp(requestTokenApiWrap.expirationDate)

        val newRequestTokenDaoWrap =
            RequestTokenDaoWrap(expirationTimeStamp, requestTokenApiWrap.requestToken)

        requestTokenDao.insertRequestToken(newRequestTokenDaoWrap)

        return@withContext async { AppResult(isSuccess = true, data = true) }
    }
}

