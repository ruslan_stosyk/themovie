package com.ruslanstosyk.themovie.data.dao

import androidx.room.Database
import androidx.room.RoomDatabase
import com.ruslanstosyk.themovie.data.dao.model.MovieDaoWrap
import com.ruslanstosyk.themovie.data.dao.model.RequestTokenDaoWrap

@Database(entities = [RequestTokenDaoWrap::class, MovieDaoWrap::class], version = 1)
abstract class TheMovieDatabase : RoomDatabase() {

    abstract fun requestTokenDao(): RequestTokenDao
    abstract fun requestMovieDab(): MovieDao
}