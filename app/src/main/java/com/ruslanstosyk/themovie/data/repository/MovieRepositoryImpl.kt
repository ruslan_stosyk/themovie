package com.ruslanstosyk.themovie.data.repository

import com.ruslanstosyk.themovie.R
import com.ruslanstosyk.themovie.data.api.RestApiService
import com.ruslanstosyk.themovie.data.api.model.Result
import com.ruslanstosyk.themovie.data.dao.MovieDao
import com.ruslanstosyk.themovie.data.dao.model.MovieDaoWrap
import com.ruslanstosyk.themovie.data.model.AppResult
import com.ruslanstosyk.themovie.data.model.Movie
import com.ruslanstosyk.themovie.data.model.MovieInfo
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.withContext

class MovieRepositoryImpl(
    private val restApiService: RestApiService,
    private val movieDao: MovieDao
) : BaseRepository(),
    MovieRepository {

    override suspend fun makeSearchAsync(query: String)
        : Deferred<AppResult<List<MovieInfo>>> = withContext(Dispatchers.IO) {
        return@withContext async {
            val response = getResult { restApiService.makeSearch(query) }
            if (response is Result.Success) {
                val movieImApiWrapList = response.data.movieInfoApiWrapList
                if (movieImApiWrapList.isNullOrEmpty()) {
                    AppResult(
                        isSuccess = false,
                        errorId = R.string.search_result_empty,
                        subError = query,
                        data = ArrayList()
                    )
                } else {
                    val moveInfoList: MutableList<MovieInfo> = movieImApiWrapList.map {
                        MovieInfo(it.id, it.posterPath ?: "", it.originalTitle ?: "")
                    }.toMutableList()
                    AppResult<List<MovieInfo>>(data = moveInfoList)
                }
            } else {
                AppResult(isSuccess = false, errorText = response.toString(), data = ArrayList())
            }
        }
    }

    override suspend fun getMovieByIdAsync(movieId: Int): Deferred<AppResult<Movie>> =
        withContext(Dispatchers.IO) {
            return@withContext async {
                val response = getResult { restApiService.getMovieById(movieId) }
                if (response is Result.Success) {
                    val movieApiWrap = response.data
                    val genres =
                        movieApiWrap.genres?.map { it.name }?.toMutableList() ?: ArrayList()

                    val isFavorite = movieDao.loadMovieById(movieId)?.isFavorite ?: false
                    val movie = Movie(
                        movieApiWrap.id,
                        movieApiWrap.posterPath,
                        movieApiWrap.originalTitle,
                        movieApiWrap.overview,
                        genres,
                        movieApiWrap.releaseDate,
                        isFavorite
                    )
                    AppResult(data = movie)
                } else {
                    AppResult<Movie>(
                        isSuccess = false,
                        errorText = response.toString(),
                        data = null
                    )
                }
            }
        }

    override suspend fun deleteFromFavorite(movieId: Int) = withContext(Dispatchers.IO) {
        val movieDaoWrap = MovieDaoWrap(movieId, true)
        movieDao.deleteMovie(movieDaoWrap)
    }

    override suspend fun addToFavorite(movieId: Int) {
        val movieDaoWrap = MovieDaoWrap(movieId, true)
        movieDao.insertMovie(movieDaoWrap)
    }
}