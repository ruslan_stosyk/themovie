package com.ruslanstosyk.themovie.di

import com.ruslanstosyk.themovie.ui.splash.SplashViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val splashModule = module {

    viewModel { SplashViewModel(get()) }
}