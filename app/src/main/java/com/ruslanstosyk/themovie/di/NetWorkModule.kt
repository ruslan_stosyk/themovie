package com.ruslanstosyk.themovie.di

import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.ruslanstosyk.themovie.BuildConfig
import com.ruslanstosyk.themovie.data.api.ApiKeyInterceptor
import com.ruslanstosyk.themovie.data.api.RestApiService
import com.ruslanstosyk.themovie.utils.RestApiUtils
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val networkModule = module {

    single { provideHttpLoggingInterceptor() }
    single { provideApiKeyInterceptor() }
    single { provideGson() }
    single { provideOkHttpClient(get(), get()) }
    single { provideRetrofit(get(), get()) }
    single { provideRestApiService(get()) }
}

fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
    val interceptor = HttpLoggingInterceptor()
    interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
    return interceptor
}

fun provideApiKeyInterceptor(): ApiKeyInterceptor {
    return ApiKeyInterceptor()
}

fun provideGson(): Gson {
    return GsonBuilder()
        .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        .create()
}

fun provideOkHttpClient(
    loggingInterceptor: HttpLoggingInterceptor,
    apiKeyInterceptor: ApiKeyInterceptor
): OkHttpClient {
    val builder: OkHttpClient.Builder = OkHttpClient.Builder()
        .readTimeout(30, TimeUnit.SECONDS)
        .connectTimeout(30, TimeUnit.SECONDS)
        .addNetworkInterceptor(apiKeyInterceptor)
    if (BuildConfig.DEBUG) {
        builder.addInterceptor(loggingInterceptor)
    }
    return builder.build()
}

fun provideRetrofit(gson: Gson, okHttpClient: OkHttpClient): Retrofit {
    return Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create(gson))
        .baseUrl(RestApiUtils.BASE_URL)
        .client(okHttpClient)
        .build()
}

fun provideRestApiService(retrofit: Retrofit): RestApiService {
    return retrofit.create(RestApiService::class.java)
}

