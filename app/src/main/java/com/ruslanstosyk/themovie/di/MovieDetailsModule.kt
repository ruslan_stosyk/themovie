package com.ruslanstosyk.themovie.di

import com.ruslanstosyk.themovie.ui.home.detailsfragment.MovieDetailsViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val movieDetailsModule = module {

    viewModel { MovieDetailsViewModel(get()) }
}