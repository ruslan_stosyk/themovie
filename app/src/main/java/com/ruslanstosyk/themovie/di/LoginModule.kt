package com.ruslanstosyk.themovie.di

import com.ruslanstosyk.themovie.ui.login.LoginViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val loginModule = module {

    viewModel { LoginViewModel(get()) }
}