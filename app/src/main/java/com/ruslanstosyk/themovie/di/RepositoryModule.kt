package com.ruslanstosyk.themovie.di

import com.ruslanstosyk.themovie.data.repository.AuthRepository
import com.ruslanstosyk.themovie.data.repository.AuthRepositoryImpl
import com.ruslanstosyk.themovie.data.repository.MovieRepository
import com.ruslanstosyk.themovie.data.repository.MovieRepositoryImpl
import org.koin.dsl.module

val repositoryModule = module {
    single<AuthRepository> { AuthRepositoryImpl(get(), get()) }

    single<MovieRepository> { MovieRepositoryImpl(get(), get()) }
}