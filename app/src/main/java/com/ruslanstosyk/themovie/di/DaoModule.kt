package com.ruslanstosyk.themovie.di

import androidx.room.Room
import com.ruslanstosyk.themovie.data.dao.TheMovieDatabase
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val daoModule = module {
    single {
        Room.databaseBuilder(androidApplication(), TheMovieDatabase::class.java, "the-movie-db")
            .build()
    }

    single { get<TheMovieDatabase>().requestTokenDao() }

    single { get<TheMovieDatabase>().requestMovieDab() }
}