package com.ruslanstosyk.themovie.di

import com.ruslanstosyk.themovie.ui.home.searchfragment.SearchMovieViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val searchMovieModule = module {

    viewModel { SearchMovieViewModel(get()) }
}