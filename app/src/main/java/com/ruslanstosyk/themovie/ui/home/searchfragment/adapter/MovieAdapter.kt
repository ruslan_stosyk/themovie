package com.ruslanstosyk.themovie.ui.home.searchfragment.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.ruslanstosyk.themovie.data.model.MovieInfo
import com.ruslanstosyk.themovie.databinding.ItemMovieInfoBinding
import com.ruslanstosyk.themovie.ui.home.searchfragment.MovieAdapterListener
import com.ruslanstosyk.themovie.utils.MovieDiffCallback

class MovieAdapter(val listener: MovieAdapterListener) :

    RecyclerView.Adapter<MovieAdapter.DeviceViewHolder>() {

    private val movieInfoList: MutableList<MovieInfo> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DeviceViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return DeviceViewHolder(ItemMovieInfoBinding.inflate(inflater, parent, false))
    }

    override fun getItemCount(): Int = movieInfoList.size

    override fun onBindViewHolder(holder: DeviceViewHolder, position: Int) {
        holder.bind(movieInfoList[position])
    }

    fun addItems(newList: List<MovieInfo>) {
        val diffResult = DiffUtil.calculateDiff(
            MovieDiffCallback(
                movieInfoList,
                newList
            )
        )
        movieInfoList.clear()
        movieInfoList.addAll(newList)
        diffResult.dispatchUpdatesTo(this)
    }

    fun removeAll() {
        movieInfoList.clear()
        notifyDataSetChanged()
    }

    inner class DeviceViewHolder(private val binding: ItemMovieInfoBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(movieInfo: MovieInfo) {
            binding.movieInfo = movieInfo
            binding.listener = listener
            binding.executePendingBindings()
        }
    }
}