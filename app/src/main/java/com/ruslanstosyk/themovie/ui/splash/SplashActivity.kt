package com.ruslanstosyk.themovie.ui.splash

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.ruslanstosyk.themovie.R
import com.ruslanstosyk.themovie.ui.home.HomeActivity
import com.ruslanstosyk.themovie.ui.login.LoginActivity
import org.koin.android.viewmodel.ext.android.viewModel

class SplashActivity : AppCompatActivity() {

    private val splashViewModel: SplashViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        subscribeUI()
    }

    override fun onStart() {
        super.onStart()
        splashViewModel.checkRequestToken()
    }

    private fun subscribeUI() {
        splashViewModel.isLoggedIn.observe(this, Observer {
            val intent: Intent = if (it) {
                Intent(this, HomeActivity::class.java)
            } else {
                Intent(this, LoginActivity::class.java)
            }
            startActivity(intent)
            finish()
        })
    }
}
