package com.ruslanstosyk.themovie.ui.login

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.ruslanstosyk.themovie.R
import com.ruslanstosyk.themovie.databinding.ActivityLoginBinding
import com.ruslanstosyk.themovie.ui.home.HomeActivity
import com.ruslanstosyk.themovie.utils.afterTextChanged
import org.koin.android.viewmodel.ext.android.viewModel

class LoginActivity : AppCompatActivity() {

    private val loginViewModel: LoginViewModel by viewModel()
    private lateinit var userNameET: EditText
    private lateinit var passwordET: EditText
    private lateinit var loadingPB: ProgressBar
    private lateinit var loginBtn: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityLoginBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_login)
        initUI(binding)
        subscribeUI()
        initTextChanger()
    }

    private fun initUI(binding: ActivityLoginBinding) {
        binding.loginViewModel = loginViewModel
        binding.lifecycleOwner = this
        binding.clickHandler = LoginActivityClickHandler()

        userNameET = binding.loginActivityUsernameEt
        passwordET = binding.loginActivityPasswordEt
        loadingPB = binding.loginActivityLoading
        loginBtn = binding.loginActivityLoginBtn
    }

    private fun subscribeUI() {
        loginViewModel.loginResult.observe(this@LoginActivity, Observer {
            val loginResult = it ?: return@Observer

            loadingPB.visibility = View.GONE
            if (loginResult.errorText != null) {
                showLoginFailed(loginResult.errorText)
            } else if (loginResult.isSuccess) {
                openHomeScreen()
            }
        })
    }

    private fun initTextChanger() {
        userNameET.afterTextChanged {
            loginViewModel.loginDataChanged(
                userNameET.text.toString(),
                passwordET.text.toString()
            )
        }

        passwordET.apply {
            afterTextChanged {
                loginViewModel.loginDataChanged(
                    userNameET.text.toString(),
                    passwordET.text.toString()
                )
            }

            setOnEditorActionListener { _, actionId, _ ->
                when (actionId) {
                    EditorInfo.IME_ACTION_DONE ->
                        LoginActivityClickHandler().onLoginClick()
                }
                false
            }
        }
    }

    private fun openHomeScreen() {
        val welcome = getString(R.string.welcome)
        Toast.makeText(applicationContext, welcome, Toast.LENGTH_LONG).show()
        val intent = Intent(this, HomeActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun showLoginFailed(errorString: String) {
        Toast.makeText(applicationContext, errorString, Toast.LENGTH_SHORT).show()
    }

    override fun onDestroy() {
        super.onDestroy()
        loginViewModel.interruptRequests()
    }

    inner class LoginActivityClickHandler {
        fun onLoginClick() {
            loadingPB.visibility = View.VISIBLE
            loginViewModel.login(
                userNameET.text.toString(),
                passwordET.text.toString()
            )
        }
    }
}