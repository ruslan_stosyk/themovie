package com.ruslanstosyk.themovie.ui

import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.StringRes
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.ruslanstosyk.themovie.data.model.AppResult
import com.ruslanstosyk.themovie.utils.RestApiUtils

@BindingAdapter("app:poster")
fun setPoster(view: ImageView, posterShortUrl: String?) {
    if (posterShortUrl != null && posterShortUrl.isNotEmpty()) {
        val fullUrl: String = RestApiUtils.getFullUrlToImage(posterShortUrl)
        Glide.with(view.context)
            .load(fullUrl)
            .into(view)
    }
}

@BindingAdapter("app:errorRes")
fun setErrorRes(view: EditText, @StringRes error: Int?) {
    if (error != null) {
        view.error = view.context.getString(error)
    }
}

@BindingAdapter("app:errorRes")
fun <T> setErrorRes(view: TextView, appResult: AppResult<T>) {
    val errorId: Int? = appResult.errorId
    val errorQuery: String? = appResult.subError
    if (errorId != null) {
        if (errorQuery?.isNotEmpty() == true) {
            view.text = view.context.getString(errorId, errorQuery)
        } else {
            view.text = view.context.getString(errorId)
        }
    }
}

@BindingAdapter("app:error")
fun setError(view: TextView, error: String?) {
    if (error != null) {
        view.text = error
    }
}