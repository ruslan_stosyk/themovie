package com.ruslanstosyk.themovie.ui.home.searchfragment

interface MovieAdapterListener {
    fun onOpenMovie(movieId: Int)
}