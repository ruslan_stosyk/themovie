package com.ruslanstosyk.themovie.ui.home.detailsfragment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ruslanstosyk.themovie.R
import com.ruslanstosyk.themovie.data.model.AppResult
import com.ruslanstosyk.themovie.data.model.Movie
import com.ruslanstosyk.themovie.data.repository.MovieRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class MovieDetailsViewModel(private val movieRepository: MovieRepository) : ViewModel() {
    private val parentJob = Job()
    private val coroutineContext: CoroutineContext get() = parentJob + Dispatchers.Main
    private val scope = CoroutineScope(coroutineContext)

    private var _movieResult: MutableLiveData<AppResult<Movie>> = MutableLiveData()
    var movieResult: LiveData<AppResult<Movie>> = _movieResult

    init {
        _movieResult.value =
            AppResult(isSuccess = true, errorId = R.string.error_incorrect_id, data = null)
    }

    fun getMovieById(movieId: Int) {
        if (movieId == -1) {
            _movieResult.value = AppResult(data = null)
        }
        scope.launch {
            val movieResult = movieRepository.getMovieByIdAsync(movieId).await()
            _movieResult.postValue(movieResult)
        }
    }

    fun addToFavorite(movieId: Int) {
        scope.launch { movieRepository.addToFavorite(movieId) }
    }

    fun deleteFromFavorite(movieId: Int) {
        scope.launch { movieRepository.deleteFromFavorite(movieId) }
    }
}
