package com.ruslanstosyk.themovie.ui.home.detailsfragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.ruslanstosyk.themovie.R
import com.ruslanstosyk.themovie.databinding.FragmentMovieDetailsBinding
import org.koin.android.viewmodel.ext.android.viewModel

class MovieDetailsFragment : Fragment() {

    private var movieId: Int = -1
    private val movieDetailsViewModel: MovieDetailsViewModel by viewModel()
    private var isFavorite = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentMovieDetailsBinding =
            DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_movie_details, container, false
            )

        movieId = arguments?.let {
            val args = MovieDetailsFragmentArgs.fromBundle(it)
            args.movieID
        } ?: -1
        initUI(binding)
        subscribeUI(binding)
        return binding.root
    }

    private fun initUI(binding: FragmentMovieDetailsBinding) {
        binding.viewModel = movieDetailsViewModel
        binding.lifecycleOwner = viewLifecycleOwner
        binding.movieDetailsFragmentFavoriteSwt.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                if (!isFavorite) {
                    movieDetailsViewModel.addToFavorite(movieId)
                }
            } else {
                movieDetailsViewModel.deleteFromFavorite(movieId)
            }
        }
    }

    private fun subscribeUI(binding: FragmentMovieDetailsBinding) {
        movieDetailsViewModel.movieResult.observe(viewLifecycleOwner, Observer {
            if (it.isSuccess && it.data != null) {
                binding.movie = it.data
                isFavorite = it.data.isFavorite
            }
        })
    }

    override fun onResume() {
        super.onResume()
        movieDetailsViewModel.getMovieById(movieId)
    }
}
