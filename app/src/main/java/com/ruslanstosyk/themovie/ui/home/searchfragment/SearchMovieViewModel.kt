package com.ruslanstosyk.themovie.ui.home.searchfragment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ruslanstosyk.themovie.R
import com.ruslanstosyk.themovie.data.model.AppResult
import com.ruslanstosyk.themovie.data.model.MovieInfo
import com.ruslanstosyk.themovie.data.repository.MovieRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class SearchMovieViewModel(private val movieRepository: MovieRepository) : ViewModel() {

    private val parentJob = Job()
    private val coroutineContext: CoroutineContext get() = parentJob + Dispatchers.Main
    private val scope = CoroutineScope(coroutineContext)

    private val _appResult: MutableLiveData<AppResult<List<MovieInfo>>> = MutableLiveData()
    val appResult: LiveData<AppResult<List<MovieInfo>>> = _appResult

    init {
        _appResult.value =
            AppResult(isSuccess = false, errorId = R.string.search_field_empty, data = null)
    }

    fun makeSearch(query: String) {
        if (query.isEmpty()) {
            showError()
        } else {
            makeApiCall(query)
        }
    }

    private fun showError() {
        _appResult.value =
            AppResult(isSuccess = false, errorId = R.string.search_field_empty, data = null)
    }

    private fun makeApiCall(query: String) {
        scope.launch {
            val result = movieRepository.makeSearchAsync(query).await()
            _appResult.postValue(result)
        }
    }
}