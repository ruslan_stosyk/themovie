package com.ruslanstosyk.themovie.ui.home.searchfragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.ruslanstosyk.themovie.R
import com.ruslanstosyk.themovie.data.model.AppResult
import com.ruslanstosyk.themovie.data.model.MovieInfo
import com.ruslanstosyk.themovie.databinding.FragmentSearchMovieBinding
import com.ruslanstosyk.themovie.ui.home.searchfragment.adapter.MovieAdapter
import com.ruslanstosyk.themovie.utils.afterTextChanged
import com.ruslanstosyk.themovie.utils.hideKeyboard
import org.koin.android.viewmodel.ext.android.viewModel

class SearchMovieFragment : Fragment(), MovieAdapterListener {

    private val searchMovieViewModel: SearchMovieViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentSearchMovieBinding =
            DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_search_movie, container, false
            )
        initUI(binding)
        subscribeUI(binding)
        initTextChanger(binding)
        return binding.root
    }

    private fun initUI(binding: FragmentSearchMovieBinding) {
        binding.viewModel = searchMovieViewModel
        binding.adapter = MovieAdapter(this)
        binding.lifecycleOwner = viewLifecycleOwner
    }

    private fun subscribeUI(binding: FragmentSearchMovieBinding) {
        searchMovieViewModel.appResult.observe(
            viewLifecycleOwner,
            Observer { onDevicesUpdated(binding, it) })
    }

    private fun initTextChanger(binding: FragmentSearchMovieBinding) {
        binding.searchMovieFragmentUsernameEt.afterTextChanged {
            searchMovieViewModel.makeSearch(
                binding.searchMovieFragmentUsernameEt.text.toString()
            )
        }
    }

    private fun onDevicesUpdated(
        binding: FragmentSearchMovieBinding,
        appResult: AppResult<List<MovieInfo>>
    ) {
        if (appResult.isSuccess) {
            binding.adapter?.addItems(appResult.data ?: ArrayList())
        } else {
            binding.adapter?.removeAll()
        }
    }

    override fun onOpenMovie(movieId: Int) {
        hideKeyboard()
        val action =
            SearchMovieFragmentDirections.actionSearchMovieFragmentToMovieDetailsFragment(movieId)
        findNavController().navigate(action)
    }
}