package com.ruslanstosyk.themovie.utils

import androidx.recyclerview.widget.DiffUtil
import com.ruslanstosyk.themovie.data.model.MovieInfo

class MovieDiffCallback(
    private val oldDevices: List<MovieInfo>?,
    private val newDevices: List<MovieInfo>?
) : DiffUtil.Callback() {

    override fun getOldListSize(): Int {
        return oldDevices?.size ?: 0
    }

    override fun getNewListSize(): Int {
        return newDevices?.size ?: 0
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldDevices!![oldItemPosition].id == newDevices!![newItemPosition].id
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val newProduct = newDevices!![newItemPosition]
        val oldProduct = oldDevices!![oldItemPosition]
        return (newProduct.id == oldProduct.id
            && newProduct.originalTitle == oldProduct.originalTitle
            && newProduct.posterPath == oldProduct.posterPath)
    }
}