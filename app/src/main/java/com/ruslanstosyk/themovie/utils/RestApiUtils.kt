package com.ruslanstosyk.themovie.utils

internal object RestApiUtils {
    const val BASE_URL = "https://api.themoviedb.org/"

    private const val BASE_IMAGE_URL = "https://image.tmdb.org/t/p/w370_and_h556_bestv2"

    const val VERSION = "3"

    fun getFullUrlToImage(posterShortUrl: String): String {
        return "$BASE_IMAGE_URL$posterShortUrl"
    }
}
